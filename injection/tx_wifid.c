/*
 * Zachary Amador <zea@{u,cs}.washington.edu>
 *
 * Based on code by Daniel Halperin <dhalperi@cs.washington.edu>
 */

#include <linux/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <tx80211.h>
#include <tx80211_packet.h>

#include "util.h"

struct lorcon_packet
{
	__le16	fc;
	__le16	dur;
	u_char	addr1[6];
	u_char	addr2[6];
	u_char	addr3[6];
	__le16	seq;
	u_char	payload[0];
} __attribute__ ((packed));

/* Number of different packet lengths to use for encoding.
 * NOTE: Should be 2 or 4 */
#define NUM_PACKET_LEN 2
/* Difference between packet lengths (in bytes) */
#define PACKET_LEN_STEP (80)
/* Number of bytes of data to transmit after each preamble */
#define WIFID_PACKET_LEN 4
struct tx80211 tx;
struct tx80211_packet tx_packets[NUM_PACKET_LEN];
struct lorcon_packet *packets[NUM_PACKET_LEN];
uint32_t packet_sizes[NUM_PACKET_LEN];

#define PAYLOAD_SIZE	2000000
uint8_t *payload_buffer;

#define FREAD_SIZE	4096
#define PREAMBLE_KEY	0xFF0F

typedef enum
{
	SINGLEPACKET,
	MULTIPACKET,
	NUM_ENCODINGS
} encoding;

static void init_lorcon(char *card, char *mon)
{
	/* Parameters for LORCON */
	int drivertype = tx80211_resolvecard(card);

	/* Initialize LORCON tx struct */
	if (tx80211_init(&tx, mon, drivertype) < 0) {
		fprintf(stderr, "Error initializing LORCON: %s\n",
		        tx80211_geterrstr(&tx));
		exit(1);
	}
	if (tx80211_open(&tx) < 0 ) {
		fprintf(stderr, "Error opening LORCON interface\n");
		exit(1);
	}
}

static inline void payload_memcpy(uint8_t *dest, uint32_t length,
		uint32_t offset)
{
	uint32_t i;
	for (i = 0; i < length; ++i) {
		dest[i] = payload_buffer[(offset + i) % PAYLOAD_SIZE];
	}
}

static void tx_pkt(struct lorcon_packet *packet, struct tx80211_packet tx_packet,
                   uint32_t packet_size, uint32_t i)
{
	int32_t ret;
	payload_memcpy(packet->payload, packet_size,
	               (i*packet_size) % PAYLOAD_SIZE);

	ret = tx80211_txpacket(&tx, &tx_packet);

	if (ret < 0) {
		fprintf(stderr, "Unable to transmit packet: %s\n", tx.errstr);
		exit(1);
	}
}

static void tx_byte(uint8_t c, encoding e)
{
	static uint32_t i = 0;
	int32_t j;
//	printf("%c", c);
	if (e == MULTIPACKET && NUM_PACKET_LEN == 2) {
		for (j = 7; j >= 0; j--) {
			if (c & (0x1 << j)) { // 1 bit
				tx_pkt(packets[0], tx_packets[0],
				       packet_sizes[0], i);
				tx_pkt(packets[1], tx_packets[1],
				       packet_sizes[1], i);
			} else { // 0 bit
				tx_pkt(packets[1], tx_packets[1],
				       packet_sizes[1], i);
				tx_pkt(packets[0], tx_packets[0],
				       packet_sizes[0], i);
			}
		}
	} else if (e == SINGLEPACKET && NUM_PACKET_LEN == 2) {
		for (j = 7; j >= 0; j--) {
			if (c & (0x1 << j)) { // 1 bit
				tx_pkt(packets[1], tx_packets[1],
				       packet_sizes[1], i);
			} else { // 0 bit
				tx_pkt(packets[0], tx_packets[0],
				       packet_sizes[0], i);
			}
		}
	} else if (e == MULTIPACKET && NUM_PACKET_LEN == 4) {
		for (j = 6; j >= 0; j -= 2) {
			uint8_t m = (c & (0x3 << j)) >> j;
			uint8_t n = ~m & 0x3;
			tx_pkt(packets[m], tx_packets[m],
			       packet_sizes[m], i);
			tx_pkt(packets[n], tx_packets[n],
			       packet_sizes[n], i);
		}
	} else if (e == SINGLEPACKET && NUM_PACKET_LEN == 4) {
		for (j = 6; j >= 0; j -= 2) {
			uint8_t m = (c & (0x3 << j)) >> j;
			tx_pkt(packets[m], tx_packets[m],
			       packet_sizes[m], i);
		}
	}
	i++;
}

static void usage()
{
	printf("Usage: tx_wifid <mode: 0=my MAC, 1=injection MAC> <encoding: 0=single-packet, 1=multi-packet> <packet delay in us>\n");
	exit(1);
}

int main(int argc, char** argv)
{
	uint32_t mode;
	uint32_t i;
	uint16_t preamble = PREAMBLE_KEY;
	uint8_t byteCtr;
	char *card = "iwlwifi";
	char *mon = "mon0";
	char *buf;
	encoding enc;

	/* Parse arguments */
	if (argc != 3)
		usage();
	if (1 != sscanf(argv[2], "%u", &enc))
		usage();
	if (1 != sscanf(argv[1], "%u", &mode))
		usage();
	if (enc >= NUM_ENCODINGS)
		usage();
	if (mode > 1)
		usage();
	
	/* Initialize packet sizes */
	for (i = 0; i < NUM_PACKET_LEN; i++) {
		packet_sizes[i] = i * PACKET_LEN_STEP;
		printf("packet size %d is %d\n", i, packet_sizes[i]);
	}
	
	/* Generate packet payloads */
	printf("Generating packet payloads \n");
	payload_buffer = malloc(PAYLOAD_SIZE);
	if (payload_buffer == NULL) {
		perror("malloc payload buffer");
		exit(1);
	}
	generate_payloads(payload_buffer, PAYLOAD_SIZE);

	/* Setup the interface for lorcon */
	printf("Initializing LORCON\n");
	init_lorcon(card, mon);

	/* Initialize packets */
	for (i = 0; i < NUM_PACKET_LEN; i++) {
		tx80211_initpacket(&tx_packets[i]);
	}

	/* Allocate packets */
	for (i = 0; i < NUM_PACKET_LEN; i++) {
		packets[i] = malloc(sizeof(struct lorcon_packet) + packet_sizes[i]);
		if (!packets[i]) {
			perror("malloc packet");
			exit(1);
		}

		packets[i]->fc = (0xc4 /* CTS */ | (0x0 << 8) /* Not To-DS */);
		packets[i]->dur = 0xffff;

		if (mode == 0) {
			memcpy(packets[i]->addr1, "\x00\x16\xea\x12\x34\x56", 6);
			get_mac_address(packets[i]->addr2, mon);
			memcpy(packets[i]->addr1, "\x00\x16\xea\x12\x34\x56", 6);
		} else if (mode == 1) {
			memcpy(packets[i]->addr1, "\x00\x16\xea\x12\x34\x56", 6);
			memcpy(packets[i]->addr2, "\x00\x16\xea\x12\x34\x56", 6);
			memcpy(packets[i]->addr3, "\xff\xff\xff\xff\xff\xff", 6);
		}
		packets[i]->seq = 0;
		tx_packets[i].packet = (uint8_t *)packets[i];
		tx_packets[i].plen = sizeof(struct lorcon_packet) + packet_sizes[i];
	}

	// Allocate input buffer
	buf = malloc(FREAD_SIZE);
	if (!buf) {
		perror("malloc buffer");
		exit(1);
	}

	// Read from stdin and transmit wifid packets
	byteCtr = 0;
	printf("Transmitting packets...\n");
	while (!feof(stdin)) {
		uint32_t j;
		size_t read = fread(buf, 1, FREAD_SIZE, stdin);
		if ((read < FREAD_SIZE) && ferror(stdin)) {
			perror("file error");
			exit(1);
		}
		// Iterate through read-in bytes and tx packets
		for (j = 0; j < read; j++) {
			if (byteCtr == 0) {
				// Transmit preamble
				for (i = 0; i < 5; i++) {
					tx_byte(preamble & 0xFF, enc);
					tx_byte(preamble >> 8, enc);
				}
				printf("%hx ", preamble);
			}
			tx_byte(buf[j], enc);
			printf("%2hhx ", buf[j]);
			byteCtr = (byteCtr + 1) % WIFID_PACKET_LEN;
			if (byteCtr == 0)
				printf("\n");
		}
	}
	while (byteCtr != 0) {
		// pad out the last wifid packet
		tx_byte(0x00, enc);
		printf("%2hhx ", 0x00);
		byteCtr = (byteCtr + 1) % WIFID_PACKET_LEN;
	}
	printf("\n");

	return 0;
}

